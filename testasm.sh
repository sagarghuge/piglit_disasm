#!/bin/sh

usage="$(basename "$0") [-h] [-b -t -g] -- Script to test i965 instruction assembler tool 

where:
    -h  show this help text
    -b  Path to i965_asm binary
    -t  Path to test file/files
    -g  3 letter platform name"

while getopts ':h:b:t:g:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) binary=$OPTARG
       ;;
    t) testDir=$OPTARG
       ;;
    g) gen=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

if [ ! -z "$testDir" ] && [ ! -z "$binary" ] && [ ! -z "$gen" ]; then 
   mkdir -p test_output
   for file in $(find $testDir -name '*.g4a'); do
      if [ -s $file ]; then
         f=$(basename $file .g4a)
         d=$(dirname $file)
         $binary -o "test_output/$f.out" -i "$file" -g $gen
         if cmp "test_output/$f.out" "$d/$f.expected" 2> /dev/null; then 
            echo "Success : $file"
         else
            printf "\n\n"
            echo "Output comparison for $file"
            diff --color=always -u "$d/$f.expected" "test_output/$f.out"
            printf "\n\n"
         fi
      fi	 
   done
else
   echo "$usage"
fi
